<?php
/**
 * @file
 * Webisco Shop Integration administration page.
 */

/**
 * Webisco Shop integration configuration form.
 */
function webisco_admin() {
  $form = array();

  $form['webisco_customer_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Webisco customer ID'),
    '#default_value' => variable_get('webisco_customer_id'),
  );

  return system_settings_form($form);
}
