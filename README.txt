Webisco Shop Integration module
-------------------------------

DESCRIPTION
-----------

Webisco Shop (http://www.byterider.de/webisco/shop) is a German online shop
solution for car repair and spare part shops. Webisco is not self-hosted, but
offers the possibility to be integrated in a website either without any
customization at all, or via placing its individual parts (widgets) on your
website. This module integrates those widgets fully into Drupal.


FEATURES
--------

This module offers:

* Drupal blocks for every available Webisco widget (navigation, main content,
  cart, search form, car selector, etc)
* a centralized shop page, where every Webisco-related page request is
  directed to
* Helper functions to place deep links to cart or registration page and 
  cleaning up Webisco URL's from session parameters
* easy configuration: enter your customer ID, place the blocks into your
  regions and go!
* Variable module support (no dependency)


REQUIREMENTS
------------
This module requires the following modules:
 * Libraries API (https://drupal.org/project/libraries)


INSTALLATION
------------

1. Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
2. Create the directory "sites/all/libraries/webisco" on your webserver and
   place the webisco.inc.php file, that you should have got from Webisco,
   inside it.


CREDITS
-------

The Webisco Shop Integration module was originally developed by
Mag. Andreas Mayr (www.agoradesign.at).
