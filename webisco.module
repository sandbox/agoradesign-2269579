<?php
/**
 * @file
 * Webisco Shop Integration module file.
 */

/**
 * Implements hook_libraries_info().
 */
function webisco_libraries_info() {
  $libraries = array();

  $libraries['webisco'] = array(
    'name' => 'Webisco Shop library',
    'vendor url' => 'http://www.byterider.de/webisco/shop',
    'download url' => 'http://www.byterider.de/webisco/shop',
    // Hard-coded version number, as no version info available in lib file.
    'version' => '3.5',
    'files' => array(
      'php' => array('webisco.inc.php'),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_permission().
 */
function webisco_permission() {
  return array(
    'administer webisco' => array(
      'title' => t('Administer Webisco configuration'),
      'description' => t('Configure Webisco Shop ingetration (set customer ID,...).'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function webisco_menu() {
  $items = array();

  $items['admin/config/services/webisco'] = array(
    'title' => 'Webisco Shop',
    'description' => 'Configure Webisco Shop Integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webisco_admin'),
    'file' => 'webisco.admin.inc',
    'access arguments' => array('administer webisco'),
  );

  $items['shop'] = array(
    'title' => 'Onlineshop',
    'description' => 'Webisco online shop main page',
    'page callback' => 'webisco_page_view',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'main-menu',
    'weight' => 4,
  );

  return $items;
}

/**
 * Menu callback - displays the centralized shop page.
 */
function webisco_page_view() {
  $output = array(
    '#markup' => _webisco_get_widget_markup('hauptbereich'),
  );
  return $output;
}

/**
 * Implements hook_boot().
 */
function webisco_boot() {
  $query_params = $_GET;

  /*
   * If "shop" param is set (and not empty) in the query string parameters,
   * always redirect to shop page.
   */
  if (!empty($query_params['shop']) && $query_params['q'] != 'shop') {
    unset($query_params['q']);

    /*
     * As the drupal_goto() function is not loaded by now (within hook_boot),
     * we have to build our target URL by ourselves.
     */
    global $base_url;

    $query_string_array = array();
    foreach ($query_params as $key => $value) {
      $query_string_array[] = "$key=$value";
    }
    $target_url = "$base_url/shop?" . implode('&', $query_string_array);

    // Do redirect.
    header('Location: ' . $target_url, TRUE, 302);
    exit;
  }
}

/**
 * Implements hook_block_info().
 */
function webisco_block_info() {
  $blocks = array();
  $blocks['cart'] = array(
    'info' => t('Webisco cart block'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['copyright'] = array(
    'info' => t('Webisco copyright block'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['links'] = array(
    'info' => t('Webisco links block'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['login'] = array(
    'info' => t('Webisco login block'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['main'] = array(
    'info' => t('Webisco main shop area'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['navigation'] = array(
    'info' => t('Webisco shop navigation'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['search'] = array(
    'info' => t('Webisco search block'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['vehicle'] = array(
    'info' => t('Webisco vehicle block'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['vehicle_selection'] = array(
    'info' => t('Webisco vehicle selection block'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function webisco_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'cart':
      $block = array(
        'content' => _webisco_get_widget_markup('warenkorb'),
      );
      break;

    case 'copyright':
      $block = array(
        'content' => _webisco_get_widget_markup('copyright'),
      );
      break;

    case 'links':
      $content = _webisco_get_link_markup('agb');
      $content .= _webisco_get_link_markup('batterieverordnung');
      $content .= _webisco_get_link_markup('datenschutz');
      $content .= _webisco_get_link_markup('impressum');
      $content .= _webisco_get_link_markup('versandbedingungen');
      $content .= _webisco_get_link_markup('widerrufsbelehrung');

      $block = array(
        'content' => $content,
      );
      break;

    case 'login':
      $block = array(
        'content' => _webisco_get_widget_markup('login'),
      );
      break;

    case 'main':
      $block = array(
        'content' => _webisco_get_widget_markup('hauptbereich'),
      );
      break;

    case 'navigation':
      $widget_parameter = array(
        'layout' => 'tree',
        'angebote' => 'true',
        'baugruppen' => 'true',
      );
      $block = array(
        'content' => _webisco_get_widget_markup('navigation', $widget_parameter),
      );
      break;

    case 'search':
      $block = array(
        'content' => _webisco_get_widget_markup('suche'),
      );
      break;

    case 'vehicle':
      $block = array(
        'content' => _webisco_get_widget_markup('fahrzeug'),
      );
      break;

    case 'vehicle_selection':
      $block = array(
        'content' => _webisco_get_widget_markup('fahrzeugauswahl'),
      );
      break;

  }
  return $block;
}


/**
 * Cleans up a given Webisco shop url, e.g. for using it in a link field, etc.
 * 
 * It removes the 'webiscosession' query string parameter, adds a 'shop' query
 * string parameter and regenerates the URL based on the internal 'shop' path.
 */
function webisco_cleanup_shop_url($shop_url, $absolute_url = FALSE) {
  $url_parts = drupal_parse_url($shop_url);

  if (isset($url_parts['query']['webiscosession'])) {
    unset($url_parts['query']['webiscosession']);
  }
  $url_parts['query']['shop'] = 1;
  return url('shop', array(
    'query' => $url_parts['query'],
    'fragment' => $url_parts['fragment'],
    'absolute' => $absolute_url,
  ));
}

/**
 * Takes a given URL, and checks if it could be a valid Webisco shop URL.
 * 
 * The check is based on the assumption, that the presence of the
 * "webiscoparams" query parameter always indicates a valid shop URL.
 */
function webisco_is_valid_shop_url($shop_url) {
  $url_parts = drupal_parse_url($shop_url);
  return !empty($url_parts['query']['webiscoparams']);
}

/**
 * Provides the URL to the Webisco shopping cart page.
 */
function webisco_get_shopping_cart_url() {
  /*
   * Hard-coded Webisco URL parameter.
   * Actually there is no need for making this configurable.
   * The parameter is also exactly the same across all Webisco shops.
   */
  $webisco_params_shopping_cart_page = 'YToxOntzOjEzOiJ3ZWJpc2NvYWN0aW9uIjtzOjEzOiJzaG93d2FyZW5rb3JiIjt9';
  return _webisco_get_page_url($webisco_params_shopping_cart_page);
}

/**
 * Provides the URL to the Webisco registration page.
 */
function webisco_get_registration_page_url() {
  /*
   * Hard-coded Webisco URL parameter.
   * Actually there is no need for making this configurable.
   * The parameter is also exactly the same across all Webisco shops.
   */
  $webisco_params_registration_page = 'YToxOntzOjEzOiJ3ZWJpc2NvYWN0aW9uIjtzOjExOiJjcmVhdGVrdW5kZSI7fQ%3D%3D';
  return _webisco_get_page_url($webisco_params_registration_page);
}

/**
 * Builds correct URL to the shop page with the given webiscoparams.
 * 
 * Intended for internal use only.
 */
function _webisco_get_page_url($webiscoparams) {
  return url('shop', array('query' => array('webiscoparams' => $webiscoparams)));
}

/**
 * Returns the HTML markup for the given link type.
 * 
 * Intended for internal use only - use the dedicated blocks instead!
 */
function _webisco_get_link_markup($link_type) {
  return _webisco_get_widget_markup('link', array('typ' => $link_type));
}

/**
 * Returns the HTML markup for the given Webisco widget.
 * 
 * Intended for internal use only - use the dedicated blocks instead!
 */
function _webisco_get_widget_markup($widget_name, $widget_parameter = array(), $get_parameter = array('shop' => 1, 'q' => 'shop')) {
  $output = _webisco_get_init_markup();
  $output .= showWebiscoWidget(variable_get('webisco_customer_id'), $widget_name, $widget_parameter, $get_parameter, TRUE);
  return $output;
}

/**
 * Loads and initializes Webisco library.
 * 
 * Only for internal use and should never be called from outside this module!
 */
function _webisco_get_init_markup() {
  static $initialized = FALSE;
  $output = '';
  if (!$initialized) {
    libraries_load('webisco');
    $output = initWebisco(variable_get('webisco_customer_id'), TRUE);
    $initialized = TRUE;
  }
  return $output;
}
