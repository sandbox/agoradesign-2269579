<?php
/**
 * @file
 * Variable module hook implementations
 */

/**
 * Implements hook_variable_group_info().
 */
function webisco_variable_group_info() {
  $groups['webisco'] = array(
    'title' => 'Webisco Online Shop',
    'access' => 'administer webisco',
  );
  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function webisco_variable_info($options) {
  $variables = array();

  $variables['webisco_customer_id'] = array(
    'title' => t('Webisco customer ID', array(), $options),
    'type' => 'number',
    'group' => 'webisco',
  );

  return $variables;
}
